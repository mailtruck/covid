package updater

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"gitlab.com/mailtruck/covid/models"
)

type SeriesType int

const (
	TypeGlobalConfirmed = iota
	TypeGlobalDeaths
	TypeUSConfirmed
	TypeUSDeaths
)

func Watch(inPath string, outPath string) {
	for {
		err := AllToFile(inPath, outPath)
		if err != nil {
			fmt.Println(err)
		}
		time.Sleep(time.Minute)
	}
}

func AllToFile(inPath string, outPath string) error {
	checkPrevious := true
	previous := models.AllTS{}
	outFile := filepath.Join(outPath, "covid.json")
	r, err := os.Open(outFile)
	if err != nil {
		checkPrevious = false
		fmt.Println("can't open previous to compare")
	} else {
		err := json.NewDecoder(r).Decode(&previous)
		if err != nil {
			fmt.Println("can't parse previous as JSON")
			checkPrevious = false
		}
	}

	gc, err := GlobalConfirmedFromPath(inPath)
	if err != nil {
		panic(err)
	}
	gd, err := GlobalDeathsFromPath(inPath)
	if err != nil {
		panic(err)
	}
	uc, err := USConfirmedFromPath(inPath)
	if err != nil {
		panic(err)
	}
	ud, err := USConfirmedFromPath(inPath)
	if err != nil {
		panic(err)
	}
	if checkPrevious {
		isNew := false
		if len(previous.GlobalConfirmed) != len(gc.TS) {
			isNew = true
		}
		if len(previous.GlobalDeaths) != len(gd.TS) {
			isNew = true
		}
		if len(previous.USConfirmed) != len(uc.TS) {
			isNew = true
		}
		if len(previous.USDeaths) != len(ud.TS) {
			isNew = true
		}
		if !isNew {
			return nil
		}

	}

	all := models.AllTS{
		Version:         time.Now().In(time.UTC),
		GlobalConfirmed: TimeSeriesToSlice(gc),
		GlobalDeaths:    TimeSeriesToSlice(gd),
		USConfirmed:     TimeSeriesToSlice(uc),
		USDeaths:        TimeSeriesToSlice(ud),
	}

	bytes, err := json.Marshal(all)
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile(outFile, bytes, 0644)
	if err != nil {
		panic(err)
	}

	return nil
}

func TimeSeriesToSlice(ts models.TimeSeries) (ret []*models.Value) {
	started := false
	i := -1
	for {
		i++
		v, ok := ts.TS[i]
		if !ok && started {
			return ret
		} else if !ok {
			continue
		}
		ret = append(ret, v)
		started = true
	}
}

func GlobalConfirmedFromPath(path string) (models.TimeSeries, error) {
	return TSFromPath(path, TypeGlobalConfirmed)
}

func GlobalDeathsFromPath(path string) (models.TimeSeries, error) {
	return TSFromPath(path, TypeGlobalDeaths)
}

func USConfirmedFromPath(path string) (models.TimeSeries, error) {
	return TSFromPath(path, TypeUSConfirmed)
}

func USDeathsFromPath(path string) (models.TimeSeries, error) {
	return TSFromPath(path, TypeUSDeaths)
}

func TSFromPath(path string, seriesType SeriesType) (models.TimeSeries, error) {
	var filePath string
	switch seriesType {
	case TypeGlobalConfirmed:
		filePath = filepath.Join(path, "time_series_covid19_confirmed_global.csv")
	case TypeGlobalDeaths:
		filePath = filepath.Join(path, "time_series_covid19_deaths_global.csv")
	case TypeUSConfirmed:
		filePath = filepath.Join(path, "time_series_covid19_confirmed_US.csv")
	case TypeUSDeaths:
		filePath = filepath.Join("time_series_covid19_deaths_US.csv")
	default:
		panic("should be unreachable")
	}

	r, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	defer r.Close()
	c := csv.NewReader(r)
	line := 0
	ret := models.TimeSeries{TS: map[int]*models.Value{}}
	for {
		record, err := c.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		for i, value := range record {
			if line == 0 {
				d, err := time.Parse("1/2/06", value)
				if err != nil {
					continue
				}
				ret.TS[i] = &models.Value{
					Date: d,
				}
				continue

			}

			_, ok := ret.TS[i]
			if !ok {
				continue
			}
			v, err := strconv.Atoi(value)
			if err != nil {
				panic(err)
			}
			ret.TS[i].Total += v

		}
		line++

	}

	return ret, nil
}
