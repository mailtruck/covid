package updater

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

)

var _ = Describe("Updater", func() {
	It("gets the latest global confirmd", func(){
		latestConfirmedGlobal := 3116398
		confirmedGlobal, err := GlobalConfirmedFromPath("fixtures")
		Expect(err).To(BeNil())

		Expect(confirmedGlobal.TS[101].Total).
			To(Equal(latestConfirmedGlobal))

		})


})
