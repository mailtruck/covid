package main

import (
	"os"

	"gitlab.com/mailtruck/covid/updater"
)

func main() {
	in := os.Getenv("COVID_IN")
	if in == "" {
		in = "../COVID-19/csse_covid_19_data/csse_covid_19_time_series"
	}
	updater.Watch(in, ".")
}
