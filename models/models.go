package models

import "time"

type AllTS struct {
	Version time.Time
	GlobalConfirmed []*Value
	GlobalDeaths []*Value
	USConfirmed []*Value
	USDeaths []*Value
}

type Value struct {
	Date time.Time
	Total int
}

type TimeSeries struct {
	TS map[int]*Value
}
