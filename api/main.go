package main

import (
	"net/http"
	"os"
	"path"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/mailtruck/covid/api/loader"
	"gitlab.com/mailtruck/covid/updater"
)

var m *loader.RealManager

func main() {
	in := os.Getenv("COVID_IN")
	if in == "" {
		in = "../COVID-19/csse_covid_19_data/csse_covid_19_time_series"
	}
	out := os.Getenv("COVID_OUT")
	if out == "" {
		out = "."
	}
	// This is ugly, but we need to ensure we write the file
	// before the loader runs
	err := updater.AllToFile(in, out)
	if err != nil {
		panic(err)
	}
	go updater.Watch(in, out)

	m = &loader.RealManager{
		Loader: &loader.RealLoader{},
		Store:  &loader.RealStore{},
		Path:   path.Join(out, "covid.json"),
	}
	go m.Watch()

	e := echo.New()
	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	// Routes
	e.GET("/covid", covid)

	// Start server
	e.Logger.Fatal(e.Start(":8080"))
}

// Handler
func covid(c echo.Context) error {
	return c.JSON(http.StatusOK, m.Get())
}
