package loader_test

import (
	"encoding/json"
	"os"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	l "gitlab.com/mailtruck/covid/api/loader"
	"gitlab.com/mailtruck/covid/api/loader/loaderfakes"

	"gitlab.com/mailtruck/covid/models"
)

var _ = Describe("Loader", func() {
	var (
		earlierData models.AllTS
		laterData   models.AllTS
		store       *loaderfakes.FakeStore
		loader      *loaderfakes.FakeLoader

		manager *l.RealManager
	)
	BeforeEach(func() {
		earlierData = models.AllTS{}
		r, err := os.Open("../fixtures/out.json")
		Expect(err).To(BeNil())
		err = json.NewDecoder(r).Decode(&earlierData)
		laterData = earlierData
		laterData.Version = laterData.Version.AddDate(0, 0, 1)
		loader = &loaderfakes.FakeLoader{}
		store = &loaderfakes.FakeStore{}
		loader.FromPathReturns(&earlierData, nil)
		manager = &l.RealManager{
			Loader:  loader,
			Store:   store,
			Path:    "",
			Version: earlierData.Version,
		}
	})

	It("calls the setter when the version is new", func() {
		loader.FromPathReturns(&laterData, nil)
		err := manager.Check()
		Expect(err).To(BeNil())
		Expect(store.SetCallCount()).To(Equal(1))

	})

	It("doesn't call the setter when the version is the same", func() {
		err := manager.Check()
		Expect(err).To(BeNil())
		Expect(store.SetCallCount()).To(Equal(0))

	})

})
