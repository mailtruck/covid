package loader

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"gitlab.com/mailtruck/covid/models"
)

//go:generate counterfeiter . Store
type Store interface {
	Set(*models.AllTS)
	Get() *models.AllTS
}

//go:generate counterfeiter . Loader
type Loader interface {
	FromPath(path string) (*models.AllTS, error)
}

type Manager interface {
	Check() error
	Watch()
}

type RealManager struct {
	Loader
	Store
	Path    string
	Version time.Time
}

type RealLoader struct{}

type RealStore struct {
	Data *models.AllTS
}

func (s *RealStore) Get() *models.AllTS {
	return s.Data
}

func (s *RealStore) Set(data *models.AllTS) {
	s.Data = data
}

func (m *RealManager) Watch() {
	for {
		err := m.Check()
		if err != nil {
			fmt.Println(err)
		}
	}
}

func (m *RealManager) Check() error {
	data, err := m.Loader.FromPath(m.Path)
	if err != nil {
		return err
	}
	if !data.Version.After(m.Version) {
		return nil
	}
	m.Version = data.Version
	m.Store.Set(data)
	return nil
}

func (l *RealLoader) FromPath(path string) (*models.AllTS, error) {
	ret := models.AllTS{}
	r, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	err = json.NewDecoder(r).Decode(&ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}
